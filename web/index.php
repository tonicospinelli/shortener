<?php

require_once __DIR__ . '/../vendor/autoload.php';

$configuration = require_once __DIR__ . '/../config/prod.php';
$application = require __DIR__ . '/../app.php';
$application->run();