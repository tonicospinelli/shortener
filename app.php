<?php

$application = new \Shortener\Silex\Application($configuration);
$application->setup();

return $application;