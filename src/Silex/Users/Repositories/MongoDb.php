<?php

namespace Shortener\Silex\Users\Repositories;

use MongoDB\Client;
use Psr\Log\LoggerInterface;
use Shortener\Users\Exception;
use Shortener\Users\Repository;
use Shortener\Users\User;

class MongoDb implements Repository
{
    const DATABASE_NAME = 'chaordic';

    const COLLECTION_NAME = 'users';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \MongoDB\Collection
     */
    private $collection;

    /**
     * SqlRepository constructor.
     *
     * @param Client          $client
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->collection = $client->selectCollection(self::DATABASE_NAME, self::COLLECTION_NAME);
        $this->logger = $logger;
    }

    public function create(User $user)
    {
        $this->collection->insertOne(['_id' => $user->id]);

        $this->logger->info('a new user was added', ['id' => $user->id]);
    }

    public function find(User $searchUser)
    {
        $this->logger->info('trying to find a user by id', ['id' => $searchUser->id]);

        $document = $this->collection->findOne(['_id' => $searchUser->id]);

        if (is_null($document)) {
            throw Exception::notFound($searchUser);
        }

        return new User($document->offsetGet('_id'));
    }

    public function remove(User $user)
    {
        $this->collection->deleteOne(['_id' => $user->id]);
    }
}
