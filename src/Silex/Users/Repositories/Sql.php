<?php

namespace Shortener\Silex\Users;

use Psr\Log\LoggerInterface;
use Shortener\Users\Exception;
use Shortener\Users\Repository;
use Shortener\Users\User;

class Sql implements Repository
{
    const QUERY_ADD = 'INSERT INTO users (id) VALUES (:id);';

    const QUERY_FIND_BY_ID = "SELECT * FROM users WHERE id = :id";

    const QUERY_REMOVE_BY_ID = "DELETE FROM users WHERE id = :id";

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SqlRepository constructor.
     *
     * @param \PDO            $connection
     * @param LoggerInterface $logger
     */
    public function __construct(\PDO $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * Prepare statement to map data into object.
     *
     * @param \PDOStatement $statement
     */
    protected function prepareStatement(\PDOStatement $statement)
    {
        $statement->setFetchMode(
            \PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,
            User::class,
            ['']
        );
    }

    public function create(User $user)
    {
        $statement = $this->connection->prepare(static::QUERY_ADD);

        if (!$statement->execute(['id' => $user->id])) {
            throw new \RuntimeException('An error was occurred.');
        }

        $this->logger->info('a new user was added', ['id' => $user->id]);
    }

    public function find(User $searchUser)
    {
        $this->logger->info('trying to find a user by id', ['id' => $searchUser->id]);

        $statement = $this->connection->prepare(static::QUERY_FIND_BY_ID);
        $this->prepareStatement($statement);
        $statement->execute(['id' => $searchUser->id]);

        $user = $statement->fetch();

        if (!$user instanceof User) {
            throw Exception::notFound($searchUser);
        }

        return $user;
    }

    public function remove(User $user)
    {
        $statement = $this->connection->prepare(static::QUERY_REMOVE_BY_ID);
        $this->prepareStatement($statement);
        $statement->execute(['id' => $user->id]);
    }
}
