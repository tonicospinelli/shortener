<?php

namespace Shortener\Silex\Users;

use Shortener\Users\Exceptions\AlreadyExists as UserAlreadyExists;
use Shortener\Users\Exceptions\NotFound as UserNotFound;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Controller
{
    /**
     * @var Service
     */
    private $service;

    /**
     * Users constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function create(Request $request)
    {
        $factory = new Factory();
        $user = $factory->createFromRequest($request);

        try {
            $this->service->create($user);
        } catch (UserAlreadyExists $exception) {
            return new Response('', Response::HTTP_CONFLICT);
        }

        return new JsonResponse($user, JsonResponse::HTTP_CREATED);
    }

    public function remove($id)
    {
        $factory = new Factory();
        $user = $factory->create($id);

        try {
            $this->service->remove($user);
        } catch (UserNotFound $exception) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse('', JsonResponse::HTTP_NO_CONTENT);
    }
}
