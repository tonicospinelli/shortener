<?php

namespace Shortener\Silex\Users;

use Psr\Log\LoggerInterface;
use Shortener\Users\Repository;
use Shortener\Users\UseCases\Create;
use Shortener\Users\UseCases\Remove;
use Shortener\Users\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Service
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Service constructor.
     *
     * @param Repository               $repository
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(Repository $repository, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function create(User $user)
    {
        $useCase = new Create($this->repository, $this->eventDispatcher);
        $useCase($user);
    }

    public function remove(User $user)
    {
        $useCase = new Remove($this->repository, $this->eventDispatcher);
        $useCase($user);
    }
}
