<?php

namespace Shortener\Silex\Users;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Shortener\Silex\Application;

class Provider implements ServiceProviderInterface
{
    public function register(Container $application)
    {
        $application['shortener_user_repository'] = $application->factory(function ($app) {
            $repositoryClass = $app['shortener.users.repository.class'];
            $reflectionClass = new \ReflectionClass($repositoryClass);

            return $reflectionClass->newInstanceArgs([
                $app['database_connection'],
                $app['monolog']
            ]);
        });
        $application['shortener_user_service'] = $application->factory(function ($app) {
            return new Service($app['shortener_user_repository'], $app['dispatcher'], $app['monolog']);
        });
        $application['shortener_user_controller'] = $application->factory(function ($app) {
            return new Controller($app['shortener_user_service']);
        });
        $this->registerRoutes($application);
    }

    private function registerRoutes(Application $application)
    {
        $application->post('/users', [$application['shortener_user_controller'], 'create']);
        $application->delete('/users/{id}', [$application['shortener_user_controller'], 'remove']);
    }
}
