<?php

namespace Shortener\Silex\Users;

use Shortener\Users\Factory as UserFactory;
use Symfony\Component\HttpFoundation\Request;

class Factory extends UserFactory
{
    public function createFromRequest(Request $request)
    {
        $dataUser = $request->request->all();

        return $this->create($dataUser['id']);
    }
}
