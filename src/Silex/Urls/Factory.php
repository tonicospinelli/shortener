<?php

namespace Shortener\Silex\Urls;

use MongoDB\Model\BSONDocument;
use Shortener\Urls\Factory as UrlFactory;
use Shortener\Users\User;
use Symfony\Component\HttpFoundation\Request;

class Factory extends UrlFactory
{
    public function fromQueryResult($id, $hits, $url, $shortUrl, $userId)
    {
        return $this->create($url, new User($userId), $id, $hits, $shortUrl);
    }

    public function createFromRequest(Request $request, User $user)
    {
        $dataUrl = $request->request->all();

        return $this->create(
            $dataUrl['url'],
            $user,
            $dataUrl['id'] ?? null,
            $dataUrl['hits'] ?? 0,
            $dataUrl['shortUrl'] ?? null
        );
    }

    public function createFromBSONDocument(BSONDocument $document)
    {
        return $this->create(
            $document['url'],
            new User($document['userId']),
            $document['_id'] ?? null,
            $document['hits'] ?? 0,
            $document['shortUrl'] ?? null
        );
    }
}
