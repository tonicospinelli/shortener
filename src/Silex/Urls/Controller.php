<?php

namespace Shortener\Silex\Urls;

use Shortener\Urls\Exceptions\AlreadyExists as UrlAlreadyExists;
use Shortener\Urls\Exceptions\NotFound as UrlNotFound;
use Shortener\Urls\ShortUrlBuilder;
use Shortener\Users\Repository as UserRepository;
use Shortener\Users\Exceptions\NotFound as UserNotFound;
use Shortener\Users\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Controller
{
    /**
     * @var Service
     */
    private $service;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * Urls constructor.
     *
     * @param Service        $service
     * @param UserRepository $userRepository
     */
    public function __construct(Service $service, UserRepository $userRepository)
    {
        $this->service = $service;
        $this->userRepository = $userRepository;
        $this->factory = new Factory(new ShortUrlBuilder());
    }

    public function create(Request $request, $userid)
    {
        $user = $this->userRepository->find(new User($userid));
        $url = $this->factory->createFromRequest($request, $user);

        try {
            $this->service->create($url);
        } catch (UrlAlreadyExists $exception) {
            return new Response('', JsonResponse::HTTP_CONFLICT);
        }

        return new JsonResponse($url, JsonResponse::HTTP_CREATED);
    }

    /**
     * @param $shortUrl
     *
     * @return RedirectResponse|Response
     */
    public function redirect($shortUrl)
    {
        try {
            $url = $this->service->hitByShortUrl($shortUrl);

            return new RedirectResponse($url->url, RedirectResponse::HTTP_MOVED_PERMANENTLY);
        } catch (UrlNotFound $exception) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response('', Response::HTTP_BAD_REQUEST);
    }

    public function remove($shortUrl)
    {
        try {
            $this->service->remove($shortUrl);
        } catch (UrlNotFound $exception) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        return new Response('', JsonResponse::HTTP_NO_CONTENT);
    }

    public function topTen()
    {
        $statistics = $this->service->findTopTen();

        return new JsonResponse($statistics, JsonResponse::HTTP_OK);
    }

    public function statisticByUser($userid)
    {
        try {
            $user = $this->userRepository->find(new User($userid));
            $statistics = $this->service->findStatisticsByUser($user);

            return new JsonResponse($statistics, JsonResponse::HTTP_OK);
        } catch (UserNotFound $exception) {
            return new Response(null, JsonResponse::HTTP_NOT_FOUND);
        }
    }

    public function statisticById($id)
    {
        try {
            $statistics = $this->service->findStatisticsById($id);

            return new JsonResponse($statistics, JsonResponse::HTTP_OK);
        } catch (UrlNotFound $exception) {
            return new Response(null, JsonResponse::HTTP_NOT_FOUND);
        }
    }
}
