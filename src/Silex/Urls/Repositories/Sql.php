<?php

namespace Shortener\Silex\Urls;

use Psr\Log\LoggerInterface;
use Shortener\Urls\Exception;
use Shortener\Urls\Repository;
use Shortener\Urls\Statistics;
use Shortener\Urls\Url;
use Shortener\Users\User;

class SqlRepository implements Repository
{
    const QUERY_ADD = 'INSERT INTO users_urls (hits, url, shortUrl, userId) VALUES (:hits, :url, :shortUrl, :userId);';

    const QUERY_UPDATE_SHORT_URL = 'UPDATE users_urls SET shortUrl = :shortUrl WHERE id = :id;';

    const QUERY_UPDATE_HITS = 'UPDATE users_urls SET hits = :hits WHERE id = :id;';

    const QUERY_FIND_BY_URL_AND_USER = "SELECT * FROM users_urls WHERE url = :url AND userid = :userId";

    const QUERY_FIND_BY_SHORT_URL = "SELECT * FROM users_urls WHERE shortUrl = :shortUrl";

    const QUERY_REMOVE_BY_ID = "DELETE FROM users_urls WHERE id = :id";

    const QUERY_FIND_TOP_TEN = "SELECT * FROM users_urls ORDER BY hits DESC LIMIT 10";

    const QUERY_TOTAL_HITS = "SELECT SUM(hits) as totalHits FROM users_urls";

    const QUERY_COUNT = "SELECT COUNT(1) as urlCount FROM users_urls";

    const QUERY_FIND_BY_USER = "SELECT * FROM users_urls WHERE userId = :userId ORDER BY hits DESC";

    const QUERY_TOTAL_HITS_BY_USER = "SELECT SUM(hits) as totalHits FROM users_urls WHERE userId = :userId";

    const QUERY_COUNT_BY_USER = "SELECT COUNT(1) as urlCount FROM users_urls WHERE userId = :userId";

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * SqlRepository constructor.
     *
     * @param \PDO            $connection
     * @param Factory         $factory
     * @param LoggerInterface $logger
     */
    public function __construct(\PDO $connection, Factory $factory, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
        $this->factory = $factory;
    }

    /**
     * Prepare statement to map data into object.
     *
     * @param \PDOStatement $statement
     */
    protected function prepareStatement(\PDOStatement $statement)
    {
        $statement->setFetchMode(
            \PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE,
            Url::class,
            ['', new User('')]
        );
    }

    public function create(Url $targetUrl)
    {
        $statement = $this->connection->prepare(static::QUERY_ADD);

        $params = [
            'url' => $targetUrl->url,
            'hits' => $targetUrl->hits,
            'userId' => $targetUrl->user->id,
            'shortUrl' => $targetUrl->shortUrl,
        ];
        if (!$statement->execute($params)) {
            throw new \RuntimeException('An error was occurred.');
        }

        $targetUrl->id = $this->connection->lastInsertId();

        $this->logger->info('a new url was added', ['id' => $targetUrl->id]);
    }

    public function findByUrlAndUser(Url $searchUrl)
    {
        $this->logger->info('trying to find a user by id', ['id' => $searchUrl->id]);

        $statement = $this->connection->prepare(static::QUERY_FIND_BY_URL_AND_USER);
        $this->prepareStatement($statement);
        $statement->execute([
            'url' => $searchUrl->url,
            'userId' => $searchUrl->user->id
        ]);

        $url = $statement->fetch();

        if (!$url instanceof Url) {
            throw Exception::notFound($searchUrl);
        }

        $url->user = $searchUrl->user;

        return $url;
    }

    public function findByShortUrl($shortUrl)
    {
        $this->logger->info('trying to find a url by shortUrl', ['shortUrl' => $shortUrl]);

        $statement = $this->connection->prepare(static::QUERY_FIND_BY_SHORT_URL);
        $this->prepareStatement($statement);
        $statement->execute(['shortUrl' => $shortUrl]);

        $url = $statement->fetch();

        if (!$url instanceof Url) {
            throw Exception::notFound(new Url($shortUrl, new User('')));
        }

        $url->user = new User($url->userId);

        return $url;
    }

    public function updateHit(Url $url)
    {
        $this->logger->info('updating hits from url', ['id' => $url->id, 'hits' => $url->hits]);

        $statement = $this->connection->prepare(static::QUERY_UPDATE_HITS);

        $params = [
            'hits' => $url->hits,
            'id' => $url->id,
        ];

        if (!$statement->execute($params)) {
            throw new \RuntimeException('An error was occurred.');
        }
    }

    public function remove(Url $url)
    {
        $this->logger->info('removing url', ['id' => $url->id]);
        $statement = $this->connection->prepare(static::QUERY_REMOVE_BY_ID);
        $this->prepareStatement($statement);
        $statement->execute(['id' => $url->id]);
    }

    public function findTopTen()
    {
        $totalHits = $this->connection->query(static::QUERY_TOTAL_HITS);
        $totalHits = intval($totalHits->fetchColumn());

        $urlCount = $this->connection->query(static::QUERY_COUNT);
        $urlCount = intval($urlCount->fetchColumn());

        $statement = $this->connection->prepare(static::QUERY_FIND_TOP_TEN);
        $statement->execute();
        $urls = $statement->fetchAll(\PDO::FETCH_FUNC, [$this->factory, 'fromQueryResult']);

        return new Statistics($totalHits, $urlCount, $urls);
    }

    public function findStatisticsByUser(User $user)
    {
        $parameter = ['userId' => $user->id];

        $totalHits = $this->connection->prepare(static::QUERY_TOTAL_HITS_BY_USER);
        $totalHits->execute($parameter);
        $totalHits = intval($totalHits->fetchColumn());

        $urlCount = $this->connection->prepare(static::QUERY_COUNT_BY_USER);
        $urlCount->execute($parameter);
        $urlCount = intval($urlCount->fetchColumn());

        $statement = $this->connection->prepare(static::QUERY_FIND_BY_USER);
        $statement->execute($parameter);
        $urls = $statement->fetchAll(\PDO::FETCH_FUNC, [$this->factory, 'fromQueryResult']);

        return new Statistics($totalHits, $urlCount, $urls);
    }
}
