<?php

namespace Shortener\Silex\Urls\Repositories;

use MongoDB\Client;
use MongoDB\Database;
use MongoDB\Model\BSONDocument;
use Psr\Log\LoggerInterface;
use Shortener\Silex\Urls\Factory;
use Shortener\Urls\Exception;
use Shortener\Urls\Repository;
use Shortener\Urls\Statistics;
use Shortener\Urls\Url;
use Shortener\Users\User;

class MongoDb implements Repository
{
    const DATABASE_NAME = 'chaordic';

    const COLLECTION_NAME = 'users_url';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var Database
     */
    private $database;

    /**
     * @var \MongoDB\Collection
     */
    private $collection;

    /**
     * SqlRepository constructor.
     *
     * @param Client          $client
     * @param Factory         $factory
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, Factory $factory, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->database = $client->selectDatabase(self::DATABASE_NAME);
        $this->collection = $this->database->selectCollection(self::COLLECTION_NAME);
        $this->logger = $logger;
        $this->factory = $factory;
    }

    public function create(Url $targetUrl)
    {
        $command = [
            'findAndModify' => self::COLLECTION_NAME,
            'query' => ['_id' => self::COLLECTION_NAME . 'id'],
            'update' => ['$inc' => ['current_id' => 1]],
            'upsert' => true,
            'new' => true,
        ];

        $commandResult = $this->database->command($command);

        $cursor = new \IteratorIterator($commandResult);
        $cursor->rewind();

        $targetUrl->id = $cursor->current()['value']['current_id'];

        $this->collection->insertOne([
            '_id' => $targetUrl->id,
            'url' => $targetUrl->url,
            'hits' => $targetUrl->hits,
            'userId' => $targetUrl->user->id,
            'shortUrl' => $targetUrl->shortUrl,
        ]);

        $this->logger->info('a new url was added', ['id' => $targetUrl->id]);
    }

    public function findByUrlAndUser(Url $searchUrl)
    {
        $this->logger->info('trying to find a user by id', ['id' => $searchUrl->id]);

        $document = $this->collection->findOne([
            'url' => ['$eq' => $searchUrl->url],
            'userId' => ['$eq' => $searchUrl->user->id],
        ]);

        if (is_null($document)) {
            throw Exception::notFound($searchUrl);
        }

        return $this->factory->createFromBSONDocument($document);
    }

    public function findByShortUrl($shortUrl)
    {
        $this->logger->info('trying to find a url by shortUrl', ['shortUrl' => $shortUrl]);

        $document = $this->collection->findOne(['shortUrl' => $shortUrl]);

        if (is_null($document)) {
            throw Exception::notFound(new Url($shortUrl, new User('')));
        }

        return $this->factory->createFromBSONDocument($document);
    }

    public function updateHit(Url $url)
    {
        $this->logger->info('updating hits from url', ['id' => $url->id, 'hits' => $url->hits]);

        $this->collection->updateOne(['_id' => (int) $url->id], ['$set' => ['hits' => $url->hits]]);
    }

    public function remove(Url $url)
    {
        $this->logger->info('removing url', ['id' => $url->id]);
        $this->collection->deleteOne(['_id' => (int) $url->id]);
    }

    public function findTopTen()
    {
        $result = $this->collection->aggregate([
            ['$group' => ['_id' => 1, 'totalHits' => ['$sum' => '$hits']]]
        ]);
        $resultIterator = new \IteratorIterator($result);
        $resultIterator->rewind();
        $totalHits = intval($resultIterator->current()['totalHits']);

        $findQuery = ['url' => ['$exists' => true]];
        $urlCount = $this->collection->count($findQuery);

        $result = $this->collection->find($findQuery, ['limit' => 10, 'sort' => ['hits' => -1]]);

        $urls = array_map(function (BSONDocument $document) {
            return $this->factory->createFromBSONDocument($document);
        }, $result->toArray());

        return new Statistics($totalHits, $urlCount, $urls);
    }

    public function findStatisticsByUser(User $user)
    {
        $filter = ['userId' => $user->id];

        $result = $this->collection->aggregate([
            ['$match' => $filter],
            ['$group' => ['_id' => 1, 'totalHits' => ['$sum' => '$hits']]]
        ]);
        $resultIterator = new \IteratorIterator($result);
        $resultIterator->rewind();
        $totalHits = intval($resultIterator->current()['totalHits']);

        $findQuery = array_merge(['url' => ['$exists' => true]], $filter);
        $urlCount = $this->collection->count($findQuery);

        $result = $this->collection->find($findQuery, ['limit' => 10, 'sort' => ['hits' => -1]]);

        $urls = array_map(function (BSONDocument $document) {
            return $this->factory->createFromBSONDocument($document);
        }, $result->toArray());

        return new Statistics($totalHits, $urlCount, $urls);
    }

    public function findStatisticsById($id)
    {
        $document = $this->collection->findOne(['_id' => (int) $id]);

        if (is_null($document)) {
            throw Exception::notFound(new Url('', new User(''), $id));
        }

        return $this->factory->createFromBSONDocument($document);
    }
}
