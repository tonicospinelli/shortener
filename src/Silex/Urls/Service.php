<?php

namespace Shortener\Silex\Urls;

use Psr\Log\LoggerInterface;
use Shortener\Urls\Repository;
use Shortener\Urls\UseCases\AddHit;
use Shortener\Urls\UseCases\Create;
use Shortener\Urls\Url;
use Shortener\Urls\UseCases\Remove;
use Shortener\Users\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Service
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Service constructor.
     *
     * @param Repository               $repository
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(Repository $repository, EventDispatcherInterface $eventDispatcher, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    public function create(Url $url)
    {
        $useCase = new Create($this->repository, $this->eventDispatcher);
        $useCase($url);
    }

    public function hitByShortUrl($shortUrl)
    {
        $useCase = new AddHit($this->repository, $this->eventDispatcher);

        return $useCase($shortUrl);
    }

    public function remove($shortUrl)
    {
        $useCase = new Remove($this->repository, $this->eventDispatcher);

        return $useCase($shortUrl);
    }

    public function findTopTen()
    {
        return $this->repository->findTopTen();
    }

    public function findStatisticsByUser(User $user)
    {
        return $this->repository->findStatisticsByUser($user);
    }

    public function findStatisticsById($id)
    {
        return $this->repository->findStatisticsById($id);
    }
}
