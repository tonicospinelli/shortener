<?php

namespace Shortener\Silex\Urls;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Shortener\Silex\Application;
use Shortener\Urls\ShortUrlBuilder;
use Shortener\Urls\UrlFormatter;

class Provider implements ServiceProviderInterface
{
    public function register(Container $application)
    {
        $application['shortener_url_repository'] = $application->factory(function ($app) {
            $repositoryClass = $app['shortener.urls.repository.class'];
            $reflectionClass = new \ReflectionClass($repositoryClass);

            return $reflectionClass->newInstanceArgs([
                $app['database_connection'],
                new Factory(new ShortUrlBuilder()),
                $app['monolog']
            ]);
        });
        $application['shortener_url_service'] = $application->factory(function ($app) {
            return new Service($app['shortener_url_repository'], $app['dispatcher'], $app['monolog']);
        });
        $application['shortener_url_controller'] = $application->factory(function ($app) {
            return new Controller($app['shortener_url_service'], $app['shortener_user_repository']);
        });
        $this->registerRoutes($application);
    }

    private function registerRoutes(Application $application)
    {
        $application->post('/users/{userid}/urls', [$application['shortener_url_controller'], 'create']);
        $application->get('/users/{userid}/stats', [$application['shortener_url_controller'], 'statisticByUser']);
        $application->get('/urls/{shortUrl}', [$application['shortener_url_controller'], 'redirect']);
        $application->delete('/urls/{shortUrl}', [$application['shortener_url_controller'], 'remove']);
        $application->get('/stats', [$application['shortener_url_controller'], 'topTen']);
        $application->get('/stats/{id}', [$application['shortener_url_controller'], 'statisticById']);
    }
}
