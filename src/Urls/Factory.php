<?php

namespace Shortener\Urls;

use Shortener\Users\User;

class Factory
{
    /**
     * @var ShortUrlBuilder
     */
    private $shortUrlBuilder;

    /**
     * Factory constructor.
     *
     * @param ShortUrlBuilder $shortUrlBuilder
     */
    public function __construct(ShortUrlBuilder $shortUrlBuilder)
    {
        $this->shortUrlBuilder = $shortUrlBuilder;
    }

    /**
     * @param string $url
     * @param User   $user
     * @param string $id
     * @param int    $hits
     * @param string $shortUrl
     *
     * @return Url
     */
    public function create($url, User $user, $id = null, $hits = 0, $shortUrl = null)
    {
        $object = new Url($url, $user, $id, $hits, $shortUrl);
        $object->buildShortUrl($this->shortUrlBuilder);

        return $object;
    }
}
