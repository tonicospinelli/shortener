<?php

namespace Shortener\Urls\Exceptions;

use Shortener\Urls\Url;

class AlreadyExists extends \DomainException
{
    public function __construct(Url $url)
    {
        parent::__construct("Url($url->id) already exists.");
    }
}
