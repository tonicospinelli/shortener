<?php

namespace Shortener\Urls\Exceptions;

use Shortener\Urls\Url;

class NotFound extends \DomainException
{
    public function __construct(Url $url)
    {
        parent::__construct("Url($url->url) was not found.");
    }
}
