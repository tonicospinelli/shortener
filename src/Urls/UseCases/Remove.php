<?php

namespace Shortener\Urls\UseCases;

use Shortener\Urls\Events\UrlRemoved as UrlRemovedEvent;
use Shortener\Urls\Repository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Remove
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(Repository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke($shortUrl)
    {
        $url = $this->repository->findByShortUrl($shortUrl);
        $this->repository->remove($url);
        $this->eventDispatcher->dispatch(UrlRemovedEvent::NAME, new UrlRemovedEvent($url));
    }
}
