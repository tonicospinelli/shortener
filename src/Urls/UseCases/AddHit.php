<?php

namespace Shortener\Urls\UseCases;

use Shortener\Urls\Events\UrlWasHitted as UrlWasHittedEvent;
use Shortener\Urls\Repository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddHit
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(Repository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke($shortUrl)
    {
        $url = $this->repository->findByShortUrl($shortUrl);
        $url->incrementHit();
        $this->repository->updateHit($url);
        $this->eventDispatcher->dispatch(UrlWasHittedEvent::NAME, new UrlWasHittedEvent($url));

        return $url;
    }
}
