<?php

namespace Shortener\Urls\UseCases;

use Shortener\Urls\Events\UrlCreated as UrlCreatedEvent;
use Shortener\Urls\Exception;
use Shortener\Urls\Exceptions\NotFound as UrlNotFound;
use Shortener\Urls\Repository;
use Shortener\Urls\Url;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Create
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(Repository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Url $targetUrl)
    {
        try {
            $this->repository->findByUrlAndUser($targetUrl);
            throw Exception::alreadyExists($targetUrl);
        } catch (UrlNotFound $exception) {
            $this->repository->create($targetUrl);
            $this->eventDispatcher->dispatch(UrlCreatedEvent::NAME, new UrlCreatedEvent($targetUrl));
        }
    }
}
