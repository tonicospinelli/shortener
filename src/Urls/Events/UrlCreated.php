<?php

namespace Shortener\Urls\Events;

use Shortener\Urls\Url;
use Symfony\Component\EventDispatcher\Event;

class UrlCreated extends Event
{
    const NAME = 'url.created';

    /**
     * @var Url
     */
    public $url;

    /**
     * UrlCreated constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url;
    }
}
