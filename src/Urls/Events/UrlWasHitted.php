<?php

namespace Shortener\Urls\Events;

use Shortener\Urls\Url;
use Symfony\Component\EventDispatcher\Event;

class UrlWasHitted extends Event
{
    const NAME = 'url.was.hitted';

    /**
     * @var Url
     */
    public $url;

    /**
     * UrlCreated constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url;
    }
}
