<?php

namespace Shortener\Urls\Events;

use Shortener\Urls\Url;
use Symfony\Component\EventDispatcher\Event;

class UrlRemoved extends Event
{
    const NAME = 'url.removed';

    /**
     * @var Url
     */
    public $url;

    /**
     * UrlCreated constructor.
     *
     * @param Url $url
     */
    public function __construct(Url $url)
    {
        $this->url = $url;
    }
}
