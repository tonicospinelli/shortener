<?php

namespace Shortener\Urls;

use Shortener\Urls\Exceptions\NotFound;
use Shortener\Users\User;

interface Repository
{
    public function create(Url $targetUrl);

    /**
     * @param Url $targetUrl
     *
     * @return Url
     *
     * @throws NotFound
     */
    public function findByUrlAndUser(Url $targetUrl);

    /**
     * @param string $shortUrl
     *
     * @return Url
     *
     * @throws NotFound
     */
    public function findByShortUrl($shortUrl);

    /**
     * @param Url $url
     *
     * @return void
     *
     * @throws NotFound
     */
    public function updateHit(Url $url);

    /**
     * @param Url $url
     *
     * @return void
     */
    public function remove(Url $url);

    /**
     * @return Statistics
     */
    public function findTopTen();

    /**
     * @param User $user
     *
     * @return Statistics
     */
    public function findStatisticsByUser(User $user);

    /**
     * @param string $id
     *
     * @return Statistics
     */
    public function findStatisticsById($id);
}
