<?php

namespace Shortener\Urls;

use Shortener\Users\User;

class Url implements \JsonSerializable
{
    const PREFIX = 'http://<host>:<port>/';

    public $id;

    public $hits;

    public $url;

    public $shortUrl;

    /**
     * @var User
     */
    public $user;

    /**
     * Url constructor.
     *
     * @param      $url
     * @param User $user
     * @param      $id
     * @param int  $hits
     * @param null $shortUrl
     */
    public function __construct($url, User $user, $id = null, $hits = 0, $shortUrl = null)
    {
        $this->id = (string) $id;
        $this->hits = (int) $hits;
        $this->url = $url;
        $this->shortUrl = $shortUrl;
        $this->user = $user;
    }

    public function buildShortUrl(ShortUrlBuilder $builder)
    {
        if (empty($this->shortUrl)) {
            $this->shortUrl = $builder->build($this->url);
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'hits' => $this->hits,
            'url' => $this->url,
            'shortUrl' => static::PREFIX . $this->shortUrl,
        ];
    }

    public function incrementHit()
    {
        $this->hits += 1;
    }
}
