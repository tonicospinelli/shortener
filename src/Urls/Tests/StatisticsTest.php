<?php

namespace Shortener\Urls\Tests;

use Shortener\Urls\Statistics;
use Shortener\Urls\Url;
use Shortener\Users\User;

class StatisticsTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldCreateAStatisticObject()
    {
        $statistic = new Statistics(1, 10, [$this->prophesize(Url::class)->reveal()]);

        $this->assertEquals(1, $statistic->hits);
        $this->assertEquals(10, $statistic->urlCount);
        $this->assertNotEmpty($statistic->urls);
    }

    public function testShouldJsonSerializeStatisticObject()
    {
        $statistic = new Statistics(1, 10, [new Url('http://test.com', new User('test'))]);

        $this->assertJson(
            '{"hits":1,"urlCount":10,"topUrls":[{"id":"","hits":0,"url":"http:\/\/test.com","shortUrl":"http:\/\/<host>:<port>\/"}]}',
            json_encode($statistic)
        );
    }
}
