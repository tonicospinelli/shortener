<?php

namespace Shortener\Urls\Tests;

use Shortener\Urls\ShortUrlBuilder;

class ShortUrlBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldBuildAShortUrlCode()
    {
        $builder = new ShortUrlBuilder();
        $hash = $builder->build('id');
        $this->assertNotEmpty($hash);
        $this->assertEquals(9,strlen($hash));
    }
}
