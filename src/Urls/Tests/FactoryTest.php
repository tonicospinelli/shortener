<?php

namespace Shortener\Urls\Tests;

use Shortener\Urls\Factory;
use Shortener\Urls\ShortUrlBuilder;
use Shortener\Urls\Url;
use Shortener\Users\User;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldCreateUrlInstance()
    {
        $builder = $this->prophesize(ShortUrlBuilder::class);
        $builder->build('http://test.com')->willReturn('hash');

        $factory = new Factory($builder->reveal());

        $user = $this->prophesize(User::class)->reveal();
        $url = $factory->create('http://test.com', $user);

        $builder->build('http://test.com')->shouldBeCalled();

        $this->assertInstanceOf(Url::class, $url);
        $this->assertEquals('http://test.com', $url->url);
        $this->assertNotEmpty($url->shortUrl);
    }
}
