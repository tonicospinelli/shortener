<?php

namespace Shortener\Urls\Tests\UseCases;

use Prophecy\Argument;
use Shortener\Urls\Events\UrlWasHitted as UrlWasHittedEvent;
use Shortener\Urls\Repository;
use Shortener\Urls\Url;
use Shortener\Urls\UseCases\AddHit;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddHitTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldSuccessOnInvokeAddHitUseCase()
    {
        $url = $this->prophesize(Url::class);

        $repository = $this->prophesize(Repository::class);
        $repository->findByShortUrl(Argument::any())->willReturn($url->reveal());
        $repository->updateHit($url)->willReturn(null);

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

        $useCase = new AddHit($repository->reveal(), $eventDispatcher->reveal());
        $useCase($url);

        $url->incrementHit()->shouldBeCalled();
        $repository->updateHit($url->reveal())->shouldBeCalled();
        $eventDispatcher->dispatch(UrlWasHittedEvent::NAME, Argument::type(UrlWasHittedEvent::class))->shouldBeCalled();
    }
}
