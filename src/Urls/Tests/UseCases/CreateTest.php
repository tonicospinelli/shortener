<?php

namespace Shortener\Urls\Tests\UseCases;

use Prophecy\Argument;
use Shortener\Urls\Events\UrlCreated;
use Shortener\Urls\Exception;
use Shortener\Urls\Exceptions\AlreadyExists;
use Shortener\Urls\Repository;
use Shortener\Urls\Url;
use Shortener\Urls\UseCases\Create;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldSuccessOnInvokeCreateUseCase()
    {
        $url = $this->prophesize(Url::class)->reveal();

        $repository = $this->prophesize(Repository::class);
        $repository->findByUrlAndUser($url)->willThrow(Exception::notFound($url));
        $repository->create($url)->willReturn(null);

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

        $useCase = new Create($repository->reveal(), $eventDispatcher->reveal());
        $useCase($url);

        $repository->create($url)->shouldBeCalled();
        $eventDispatcher->dispatch(UrlCreated::NAME, Argument::type(UrlCreated::class))->shouldBeCalled();
    }

    public function testShouldFailedInvokeCreateUseCase()
    {
        $this->expectException(AlreadyExists::class);

        $url = $this->prophesize(Url::class)->reveal();

        $repository = $this->prophesize(Repository::class);
        $repository->findByUrlAndUser($url)->willReturn($url);

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

        $useCase = new Create($repository->reveal(), $eventDispatcher->reveal());
        $useCase($url);
    }
}
