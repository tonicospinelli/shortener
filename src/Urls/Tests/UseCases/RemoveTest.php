<?php

namespace Shortener\Urls\Tests\UseCases;

use Prophecy\Argument;
use Shortener\Urls\Events\UrlRemoved as UrlRemovedEvent;
use Shortener\Urls\Repository;
use Shortener\Urls\Url;
use Shortener\Urls\UseCases\Remove;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RemoveTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldSuccessOnInvokeRemoveUseCase()
    {
        $url = $this->prophesize(Url::class);

        $repository = $this->prophesize(Repository::class);
        $repository->findByShortUrl(Argument::any())->willReturn($url->reveal());
        $repository->remove($url)->willReturn(null);

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);

        $useCase = new Remove($repository->reveal(), $eventDispatcher->reveal());
        $useCase($url);

        $repository->remove($url->reveal())->shouldBeCalled();
        $eventDispatcher->dispatch(UrlRemovedEvent::NAME, Argument::type(UrlRemovedEvent::class))->shouldBeCalled();
    }
}
