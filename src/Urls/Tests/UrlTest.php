<?php

namespace Shortener\Urls\Tests;

use Shortener\Urls\ShortUrlBuilder;
use Shortener\Urls\Url;
use Shortener\Users\User;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldCreateAnDefaultInstance()
    {
        $url = new Url('http://test.com', $this->prophesize(User::class)->reveal());
        $this->assertEquals('http://test.com', $url->url);
        $this->assertEquals(0, $url->hits);
        $this->assertInstanceOf(User::class, $url->user);
    }

    public function testShouldIncrementAHit()
    {
        $url = new Url('http://test.com', $this->prophesize(User::class)->reveal());
        $this->assertEquals(0, $url->hits);

        $url->incrementHit();
        $this->assertEquals(1, $url->hits);
    }

    public function testShouldCreateShorUrl()
    {
        $url = new Url('http://test.com', $this->prophesize(User::class)->reveal());

        $builder = $this->prophesize(ShortUrlBuilder::class);
        $builder->build('http://test.com')->willReturn('short-url');
        $url->buildShortUrl($builder->reveal());

        $this->assertEquals('short-url', $url->shortUrl);
    }

    public function testShouldSerializeToJson()
    {
        $url = new Url(
            'http://test.com',
            $this->prophesize(User::class)->reveal(),
            10,
            999,
            'jahsdiuahj'
        );

        $this->assertJson(
            '{"id":"10","hits":999,"url":"http:\/\/test.com","shortUrl":"http:\/\/<host>:<port>\/jahsdiuahj"}',
            json_encode($url)
        );
    }
}
