<?php

namespace Shortener\Urls;

use Ramsey\Uuid\Uuid;

class ShortUrlBuilder
{
    public function build($id)
    {
        return $this->convert($id);
    }

    private function convert($id)
    {
        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS, $id)->getHex();

        return substr($uuid, 0, 9);
    }
}
