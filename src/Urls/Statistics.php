<?php

namespace Shortener\Urls;

class Statistics implements \JsonSerializable
{
    /**
     * @var int
     */
    public $hits;

    /**
     * @var int
     */
    public $urlCount;

    /**
     * @var \Countable
     */
    public $urls;

    /**
     * Statistics constructor.
     *
     * @param int   $hits
     * @param int   $urlCount
     * @param array $urls
     */
    public function __construct($hits, $urlCount, array $urls)
    {
        $this->hits = $hits;
        $this->urlCount = $urlCount;
        $this->urls = $urls;
    }

    public function jsonSerialize()
    {
        return [
            'hits' => $this->hits,
            'urlCount' => $this->urlCount,
            'topUrls' => $this->urls,
        ];
    }
}
