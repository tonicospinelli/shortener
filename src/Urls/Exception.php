<?php

namespace Shortener\Urls;

use Shortener\Urls\Exceptions\AlreadyExists;
use Shortener\Urls\Exceptions\NotFound;

class Exception
{
    public static function alreadyExists(Url $url)
    {
        return new AlreadyExists($url);
    }

    public static function notFound(Url $url)
    {
        return new NotFound($url);
    }
}
