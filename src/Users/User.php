<?php

namespace Shortener\Users;

use Shortener\Users\Exceptions\IdNotEmpty;

class User implements \JsonSerializable
{
    public $id;

    /**
     * User constructor.
     *
     * @param string $id
     *
     * @throws IdNotEmpty
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
        ];
    }
}
