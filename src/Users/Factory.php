<?php

namespace Shortener\Users;

class Factory
{
    /**
     * @param string $id
     *
     * @return User
     */
    public function create($id)
    {
        return new User($id);
    }
}
