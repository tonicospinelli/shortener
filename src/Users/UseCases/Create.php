<?php

namespace Shortener\Users\UseCases;

use Shortener\Users\Exception;
use Shortener\Users\Exceptions\NotFound as UserNotFound;
use Shortener\Users\Repository as UserRepository;
use Shortener\Users\Events\UserCreated as UserCreatedEvent;
use Shortener\Users\Exception as UserException;
use Shortener\Users\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Create
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(UserRepository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(User $user)
    {
        if (empty($user->id)) {
            throw Exception::idNotEmpty();
        }

        try {
            $this->repository->find($user);
            throw UserException::alreadyExists($user);
        } catch (UserNotFound $exception) {
            $this->repository->create($user);
            $this->eventDispatcher->dispatch(UserCreatedEvent::NAME, new UserCreatedEvent($user));
        }
    }
}
