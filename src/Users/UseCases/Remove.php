<?php

namespace Shortener\Users\UseCases;

use Shortener\Users\Repository as UserRepository;
use Shortener\Users\Events\UserRemoved as UserRemovedEvent;
use Shortener\Users\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Remove
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(UserRepository $repository, EventDispatcherInterface $eventDispatcher)
    {
        $this->repository = $repository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(User $targetUser)
    {
        $user = $this->repository->find($targetUser);
        $this->repository->remove($user);
        $this->eventDispatcher->dispatch(UserRemovedEvent::NAME, new UserRemovedEvent($user));
    }
}
