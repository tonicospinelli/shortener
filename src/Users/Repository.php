<?php

namespace Shortener\Users;

interface Repository
{
    /**
     * @param User $user
     *
     * @return void
     */
    public function create(User $user);

    /**
     * @param User $user
     *
     * @return User
     */
    public function find(User $user);

    /**
     * @param User $user
     *
     * @return User
     */
    public function remove(User $user);
}
