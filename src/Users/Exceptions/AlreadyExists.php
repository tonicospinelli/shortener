<?php

namespace Shortener\Users\Exceptions;

use Shortener\Users\User;

class AlreadyExists extends \DomainException
{
    public function __construct(User $user)
    {
        parent::__construct("User($user->id) already exists.");
    }
}
