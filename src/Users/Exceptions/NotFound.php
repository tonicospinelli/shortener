<?php

namespace Shortener\Users\Exceptions;

use Shortener\Users\User;

class NotFound extends \DomainException
{
    public function __construct(User $user)
    {
        parent::__construct("User($user->id) was not found.");
    }
}
