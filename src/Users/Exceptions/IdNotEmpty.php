<?php

namespace Shortener\Users\Exceptions;

class IdNotEmpty extends \DomainException
{
    public function __construct()
    {
        parent::__construct('The id must be not empty.');
    }
}
