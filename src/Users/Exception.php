<?php

namespace Shortener\Users;

use Shortener\Users\Exceptions\AlreadyExists;
use Shortener\Users\Exceptions\IdNotEmpty;
use Shortener\Users\Exceptions\NotFound;

class Exception
{
    public static function alreadyExists(User $user)
    {
        return new AlreadyExists($user);
    }

    public static function notFound(User $user)
    {
        return new NotFound($user);
    }

    public static function idNotEmpty()
    {
        return new IdNotEmpty();
    }
}
