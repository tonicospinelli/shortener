<?php

namespace Shortener\Users\Events;

use Shortener\Users\User;
use Symfony\Component\EventDispatcher\Event;

class UserRemoved extends Event
{
    const NAME = 'user.removed';

    /**
     * @var User
     */
    public $user;

    /**
     * UserCreated constructor.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
